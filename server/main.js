import { Meteor } from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base'
import '../import/API/users'
Meteor.startup(() => {
  // code to run on server at startup
  Accounts.onCreateUser(function(options, user){
    user.dates = [];
    user.isAdmin=false;
    return user;
});
Meteor.publish("myuser",
  function () {
    if(this.userId)
    {
    return Meteor.users.find(this.userId)
    }
    else{
    return  this.ready()
    }
  }
);
});

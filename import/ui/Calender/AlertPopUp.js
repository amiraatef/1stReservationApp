import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';

/**
 * Alerts are urgent interruptions, requiring acknowledgement, that inform the user about a situation.
 */
export default class AlertPopUp extends React.Component {


  state = {
    open: false
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  


  render() {
    const actions = [
      <FlatButton
        label="Discard"
        primary={true}
        onClick={this.props.closed}
      />
    ];

    return (
      <div>
        {/* <RaisedButton label="Alert" onClick={this.handleOpen} /> */}
        <Dialog
          actions={actions}
          modal={false}
          open={this.props.open}
          onRequestClose={this.handleClose}
        >
         {this.props.msg}
        </Dialog>
      </div>
    );
  }
}
import React, { Component } from 'react'
import {App} from './Calender/CalenderApp'
import {Routes} from '../Routes/routes'
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import rootreducer from './reducer/index';

const store = createStore(rootreducer);
export default class Reservation extends Component {
  render() {
    return (
      <div>
            <Provider store={store}>
       <MuiThemeProvider>
       <Routes/> 
       </MuiThemeProvider>
        </Provider>
      </div>
    )
  }
}

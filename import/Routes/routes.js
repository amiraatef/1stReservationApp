import { Meteor } from 'meteor/meteor';
import React from 'react';
import ReactDOM from 'react-dom'
import { Route, Switch , BrowserRouter as Router } from 'react-router-dom';
import { Signup } from '../ui/Signup';
import  Login  from '../ui/Login';
import { AdminPage } from '../ui/Admin';
import  App from '../ui/Calender/CalenderApp'
import createHistory from 'history/createBrowserHistory'
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import rootreducer from '../ui/reducer/index';
const store = createStore(rootreducer)
const history = createHistory()



const unauthenticatedPages = ['/', '/signup'];
const authenticatedPages = ['/admin','/Reserve'];



const onEnterPrivatePage = () => {
{


  if(Meteor.loggingIn())
  {
    Meteor._debug()
    debugger;
    console.log(Meteor.user())

    if(Meteor.user().isAdmin==true)
    {
      Meteor.subscribe('myuser') 
      console.log(Meteor.user())
  
      history.replace('/admin');
  
    }
    else{
      Meteor.subscribe('myuser') 
      console.log(Meteor.user())
      history.replace('/Reserve');
  
    }
  }
  
};
}

export const onAuthChange = (isAuthenticated) => {

  const pathname = history.location.pathname;
  const isAuthenticatedPage = authenticatedPages.includes(pathname);
  if (isAuthenticatedPage && !isAuthenticated) {
    history.replace('/');
  }
};




export const routes = (
<Provider store={store}>
    <Router history={history}>
      <Switch>
        <div>
        <Route exact path="/" component={Login} onEnter={onEnterPrivatePage} />
        <Route path="/signup" component={Signup} onEnter={onEnterPrivatePage} />
        <Route path="/admin" component={AdminPage} onEnter={onEnterPrivatePage}/>
        <Route path="/Reserve" component={App} onEnter={onEnterPrivatePage}/>
        </div>
        {/* <Route  exact path="*" component={NotFound}/> */}
      </Switch>
    </Router>
    </Provider>
);
import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';
import { Accounts } from 'meteor/accounts-base';
import moment from 'moment'

Accounts.validateNewUser((user) => {
  const email = user.emails[0].address;


  new SimpleSchema({
    email: {
      type: String,
      regEx: SimpleSchema.RegEx.Email
    }
  }).validate({ email });


  return true;
});

Meteor.users.allow({
  insert: function (userId, doc) {
    return true;
  }
  ,
  update: function (userId, doc, fieldNames, modifier) {
    return true;
  }
  ,
  remove: function (userId, doc) {
    return true;
  }


});

Meteor.methods({


  'addReservation'(NewDate) {
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    else {
      //validation on date 
      debugger;
      Meteor.users.update({ _id: this.userId }, { $addToSet: { dates: NewDate } })
    }
  },

  'addToAdmin'(NewDate) {
    let Admin = Meteor.users.findOne({ isAdmin: true })
    console.log(Admin._id)
    Meteor.users.update({ _id: Admin._id }, { $addToSet: { dates: NewDate } })

  },
  'cancelledDate'(date) {
    Meteor.users.update({ _id: date.userID }, { $pull: { "dates": { "ID": date.ID } } })

    Meteor.users.update({ _id: Meteor.userId() }, { $pull: { "dates": { "ID": date.ID } } })
    console.log(Meteor.user())

  },
  'approvedDate'(date) {

    Meteor.users.update({ _id: date.userID, 'dates.ID': date.ID }, { $set: { "dates.$.Status": "approved" } });
    let current = Meteor.users.find({ _id: date.userID }).fetch();
    console.log("User  .............. ", current)
    Meteor.users.update({ _id: Meteor.userId() }, { $pull: { "dates": { "ID": date.ID } } })


  },

  /*
  
      
      { CustomerName: 'koko',
  ZoneNumber: '2',
 FromTime: 2018-06-13T15:23:11.699Z,
  ToTime: 2018-06-13T15:56:20.440Z,
  currentData: '2018-06-13T00:47:56+02:00' }
      
  */

  'CheckAvailablity'(values) {
    //console.log("users",JSON.stringify(Meteor.users.find().fetch(),undefined,2))
   const users= Meteor.users.find().fetch();
   console.log(users)
    for (let key in users) {
   console.log("user in Db ",users[key])
   Meteor._debug()
   debugger;
      if (!users[key].isAdmin) {
        
          for (let index in users[key].dates)
           {

            console.log("Zones",users[key].dates[index].zone, values.ZoneNumber)

          if (users[key].dates[index].zone == values.ZoneNumber) {

            var TimeStart = moment( values.FromTime)
            var TimeEnd = moment( values.ToTime)
            console.log("TimeStart", TimeStart)
            console.log("TimeEnd", TimeEnd)
            console.log("user.dates[key].OldFrom", moment( users[key].dates[index].OldFrom))
            console.log("user.dates[key].oldTo",  moment(users[key].dates[index].oldTo))

            if (  moment( users[key].dates[index].OldFrom).isBetween(TimeStart, TimeEnd) || moment( users[key].dates[index].oldTo).isBetween(TimeStart, TimeEnd)) {
             return "has to anothr user "

            }




          }
        }
      }
    }
  }
})